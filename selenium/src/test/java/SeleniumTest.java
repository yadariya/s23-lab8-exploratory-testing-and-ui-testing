import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SeleniumTest {

    @Test
    public void test() throws InterruptedException {
        WebDriver driver;
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://translate.google.com/");
        driver.findElement(By
                        .xpath("//*[@id=\"yDmH0d\"]/c-wiz/div/div[2]/c-wiz/div[2]" +
                                "/c-wiz/div[1]/div[2]/div[3]/c-wiz[1]/span/span/div/textarea"))
                .sendKeys("компьютер");
        boolean resultComputer = driver.getPageSource().contains("computer");
        Assert.assertTrue(resultComputer);
        driver.quit();
    }
}
